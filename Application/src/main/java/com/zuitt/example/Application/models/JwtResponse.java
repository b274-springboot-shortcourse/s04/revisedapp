package com.zuitt.example.Application.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L; //pag negative descending eencrypt

//    final is like const meaning hindi mapapalitan yung value; final na yung data; not allowed to modify
    private final String jwttoken;

//    constructor
    public JwtResponse(String jwttoken){
        this.jwttoken = jwttoken;
    }

//  getter
    public String getToken(){
        return this.jwttoken;
    }





}

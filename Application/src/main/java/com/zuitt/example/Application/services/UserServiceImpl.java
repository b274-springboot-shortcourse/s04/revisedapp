package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;
import com.zuitt.example.Application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// Annotation
@Service


// All queries will be done here; like controller in React
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

//    Create user
    public void createUser(User user) {
        userRepository.save(user);
    }

//    check if existing of not
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}

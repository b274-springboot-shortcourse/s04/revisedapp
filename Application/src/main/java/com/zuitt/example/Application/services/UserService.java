package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;

import java.util.Optional;

public interface UserService {
//    void - if walang return; remove void if need ng return
    void createUser(User user);

//   Optional - checks if may marereturn or wala; existing or non-existing
    Optional<User> findByUsername(String name);




}

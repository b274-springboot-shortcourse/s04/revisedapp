package com.zuitt.example.Application.services;

import com.zuitt.example.Application.models.User;
import com.zuitt.example.Application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {
//    autowired so flawless ang pagpasok and paglabas ng data
    @Autowired
    private UserRepository userRepository;

//    override - use para okay lang na magkakapareho yung ex: findByUsername kahit magkakapareho lang
//    throws - returns error message
//    throws - pag gagawa pa lang ng error message
//    throw pag gagamitin na

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        User user = userRepository.findByUsername(username);

        if(user == null){
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }







}

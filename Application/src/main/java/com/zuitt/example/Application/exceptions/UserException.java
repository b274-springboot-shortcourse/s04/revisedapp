package com.zuitt.example.Application.exceptions;

// no need to import java coz it's already added in the library of java
public class UserException extends Exception{
    public UserException(String message){
//  super is used to access mother class immediately
        super(message);
    }

}

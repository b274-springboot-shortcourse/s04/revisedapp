package com.zuitt.example.Application.models;

import java.io.Serializable;

public class JwtRequest implements Serializable {
    private static final long serialVersionUID = 5926468583005150707L;

    private String username;
    private String password;

//    Constructor
    public JwtRequest(){}

    public JwtRequest(String username, String password){
        this.setUsername(username);
        this.setPassword(password);
    }

//    getters
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }

//    setter
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
}
